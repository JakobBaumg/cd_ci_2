public class Car {
    private float hP;
    private float weight;

    public Car(int hP,int weight){
        this.hP=hP;
        this.weight=weight;
    }

    public float powerWeightRatio(){
        return this.hP/this.weight;
    }
}