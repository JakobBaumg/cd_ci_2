import junit.framework.TestCase;

public class CarTest extends TestCase {

    public void testPowerWeightRatio_Success() {
        Car car= new Car(250,1000);
        assertEquals(0.25f,car.powerWeightRatio());
    }

    public void testPowerWeightRatio_ForcedError(){
        Car car= new Car(250,1250);
        assertEquals(0.2f,car.powerWeightRatio());
    }
}